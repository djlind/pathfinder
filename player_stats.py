import random

class player_stats():
  """holds player attributes"""
  _race_stats = {
      'Dragonborn': {
          "strength": 2,
          "dexterity": 0,
          "constitution": 0,
          "intelligence": 0,
          "wisdom": 0,
          "charisma": 1},
      'Dwarf': {
          "strength": 0,
          "dexterity": 0,
          "constitution": 2,
          "intelligence": 0,
          "wisdom": 0,
          "charisma": 0},
      'Elf': {
          "strength": 0,
          "dexterity": 2,
          "constitution": 0,
          "intelligence": 0,
          "wisdom": 0,
          "charisma": 0},
      'Gnome': {
          "strength": 0,
          "dexterity": 0,
          "constitution": 0,
          "intelligence": 2,
          "wisdom": 0,
          "charisma": 0},
      'Half-Elf': {
          "strength": 0,
          "dexterity": 0,
          "constitution": 0,
          "intelligence": 0,
          "wisdom": 0,
          "charisma": 2},
      'Halfling': {
          "strength": 0,
          "dexterity": 2,
          "constitution": 0,
          "intelligence": 0,
          "wisdom": 0,
          "charisma": 0},
      'Half-Orc': {
          "strength": 2,
          "dexterity": 0,
          "constitution": 1,
          "intelligence": 0,
          "wisdom": 0,
          "charisma": 0},
      'Human': {
          "strength": 1,
          "dexterity": 1,
          "constitution": 1,
          "intelligence": 1,
          "wisdom": 1,
          "charisma": 1},
      'Tiefling': {
          "strength": 0,
          "dexterity": 0,
          "constitution": 0,
          "intelligence": 1,
          "wisdom": 0,
          "charisma": 2}
  }

  def __init__(self, race):
    self._base_strength = self._roll_base_stat("strength", race)
    self._base_dexterity = self._roll_base_stat("dexterity", race)
    self._base_constitution = self._roll_base_stat("constitution", race)
    self._base_intelligence = self._roll_base_stat("intelligence", race)
    self._base_wisdom = self._roll_base_stat("wisdom", race)
    self._base_charisma = self._roll_base_stat("charisma", race)

  def _select_stats(self, dice_per_attribute = [12, 6]):
    all_rolls = [
        [random.randint(1, die) for die in dice_per_attribute]
        for _ in range(6)
    ]
    all_rolls.sort(key = sum, reverse = True)
    print("Your rolls are:")
    for roll in all_rolls:
      print(roll, "Your total is :", sum(roll))
    attributes = ["strength", "dexterity", "constitution", "intelligence", "wisdom", "charisma"]
    saved_values = {}
    for roll in all_rolls:
      answer = None
      while answer is None:
        #todo: show user remain rolls to choose from
        #todo: improve dialogue
        #todo: show user sum of rolls
        print("Please select an attribute for your roll of:", roll)
        print("Attributes left to choose:", attributes)
        #todo: allow shorter input from user
        answer = input()
        if answer not in attributes:
          print("Sorry,", answer, "is not a valid selection.")
          print("Please type the exact name of the attribute you would like to",
            "assign your roll to.")
          answer = None
      saved_values[answer] = roll
      attributes.remove(answer)
    return saved_values


  def _roll_base_stat(self, stat, race):
    print("Enter the number dice and number of sides per attribute.")
    dice = input("# of dice:")
    number = input("# of sides:")
    rolls = [[random.randint(1,number) for _ in dice] for n in range(6)]
    print(
      "Your rolls are:",
      rolls[0][range(dice)],
      rolls[1][range(dice)],
      rolls[2][range(dice)],
      rolls[3][range(dice)],
      rolls[4][range(dice)],
      rolls[5][range(dice)],
      sep="\n")
    rolls = rolls.sort()
    return sum(rolls) # + self._race_stats[race][stat]