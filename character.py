# character.py
#--------RACES SECTION--------#
from player_stats import player_stats
RACES = (
    'Dragonborn',
    'Dwarf',
    'Elf',
    'Gnome',
    'Half-Elf',
    'Halfling',
    'Half-Orc',
    'Human',
    'Tiefling'
)

RACE_TRAITS = {
    'Dragonborn': {"draconic ancestry": True, "breath weapon": True, "damage resistance": True},
    'Dwarf': {"darkvision": True, "dwarven resilience": True, "dwarven combat training": True, "stonecutting":True},
    'Elf': {"darkvision": True,"keen senses": True, "fey ancestry": True, "trance": True},
    'Gnome': {"darkvision": True,"gnome cunning": True},
    'Half-Elf': {"darkvision": True, "fey ancestry": True,"skill versatility": True},
    'Halfling': {"lucky": True, "brave": True, "halfing nimbleness": True},
    'Half-Orc': {"darkvision": True, "menacing": True, "relentless endurance": True, "savage attacks":True},
    'Human': {"extra language": True},
    'Tiefling': {"darkvision": True, "hellish resistance": True, "infernal legacy": True}
}

CLASSES = (
    'Barbarian',
    'Bard',
    'Cleric',
    'Druid',
    'Fighter',
    'Monk',
    'Paladin',
    'Ranger',
    'Rogue',
    'Sorcerer',
    'Warlock',
    'Wizard'
)

def get_race():
  # Keep asking for input until you get something valid
  race = None
  while race is None:
    choice = input("Please enter the race you would like: ")
    race = validate_race(choice)
  return race

def validate_race(choice):
  """Checks whether `choice` is a valid race"""
  if choice in RACES:
    # it's valid!  return the choice
    print("Race: ", choice)
    return choice
  elif choice == "see list":
    # print the race list for the user
    print(RACES)
  else:
    # give an error
    print("Sorry, please select a race from the 5e list. To see this list, type 'see list'")

#--------CLASSES SECTION--------#

def get_class():
  # Keep asking for input until you get something valid
  game_class = None
  while game_class is None:
    choice = input("Please enter the class you would like: ")
    game_class = validate_classes(choice)
  return game_class

def validate_classes(choice):
  """Checks whether `choice` is a valid class"""
  if choice in CLASSES:
    # it's valid!  return the choice
    print("Class: ", choice)
    return choice
  elif choice == "see list":
    # print the race list for the user
    print(CLASSES)
  else:
    # give an error
    print("Sorry, please select a class from the 5e list. To see this list, type 'see list'")

#--------PLAYER CLASS--------#
class player():
  """holds basic player attributes"""
  _hit_die = {
      'Barbarian': 12,
      'Bard': 8,
      'Cleric': 8,
      'Druid': 8,
      'Fighter': 10,
      'Monk': 8,
      'Paladin': 10,
      'Ranger': 10,
      'Rogue': 8,
      'Sorcerer': 6,
      'Warlock': 8,
      'Wizard': 6
  }

  def __init__(self, gclass):
    self._hit_points = random.randint(1, _hit_die[gclass])

#--------EQUIPMENT CLASS--------#
class equipment():
  """Used to create pieces of equipment for players/npc"""
  _types = {
      'weapon': [
          'simple melee weapon',
          'simple ranged weapon',
          'martial melee weapon',
          'martial ranged weapon',
          ],
      'armor': [
          'light',
          'medium',
          'heavy',
          'sheild'
          ],
      'adv gear': [
          'ammunition',
          'arcane focus',
          'druidic focus',
          'holy symbol',
          'other'
          ],
      'tools': [
          'artisan tools',
          'gaming set',
          'musical instrument',
          'other tools'
          ],
      'transport': [
          'mounts',
          'harness/drawn vehicles',
          'saddles',
          'waterborn'
          ],
      'trade goods': [],
      'trinkets': []
  }
  _weapons = {
      'dagger': {
          'cost': 2,
          'damage': range(1,4),
          'dtype': "piercing",
          'weight': 1,
          'prop': ["finese", "light", "thrown"],
          'type': "simple melee weapon"
          },
      'shortbow': {
          'cost': 25,
          'damage': range(1,6),
          'dtype': "piercing",
          'weight': 2,
          'prop': ["ammunition", "two-handed"],
          'type': "simple ranged weapon"
          },
      'battleaxe': {
          'cost': 10,
          'damage': range(1,8),
          'dtype': "slashing",
          'weight': 4,
          'prop': ["versatile"],
          'type': "martial melee weapon"
          },
      'longbow': {
          'cost': 50,
          'damage': range(1,8),
          'dtype': "piercing",
          'weight': 2,
          'prop': ["ammunition", "heavy", "two-handed"],
          'type': "martial ranged weapon"
          }
  }

  def __init__(self, name):
    self._type = self._weapons[name]['type']
    self.cost = self._weapons[name]['cost']
    self.damage = self._weapons[name]['damage']
    self._dtype = self._weapons[name]['dtype']
    self.weight = self._weapons[name]['weight']
    self._properties = self._weapons[name]['prop']

def main():
  player_race = get_race()
  player_attributes = player_stats(player_race)
  player_class = get_class()

main()